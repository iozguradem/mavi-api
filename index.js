const express = require('express')
const app = express()
const os = require('os')
var fs = require('fs')
let items = []

app.get('/', (req, res) => {
  console.log('url:/')
  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  res.json({
    status: true,
    memory: used,
    hostname: os.hostname()
  })
})

app.get('/image', (req, res) => {
  console.log('url:/image')
  fs.readFile('image.png', 'utf8', (err, data) => {
    items.push(data)
    const used = process.memoryUsage().heapUsed / 1024 / 1024;
    res.json({
      status: true,
      memory: used,
      hostname: os.hostname()
    })
  })
})

app.listen(80, () => console.log(`Example app listening on port 80!`))